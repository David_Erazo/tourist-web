<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ADMIN_TYPE = 'admin';
    const TOURIST_TYPE = 'turista';
    const DEFAULT_TYPE = 'default';

    // Función que determina si un Usuario es admin o no
    public function isAdmin()    {     
        return $this->type === self::ADMIN_TYPE;    
    }
    // Función que determina si un Usuario es turista o no
    public function isTourist()    {        
        return $this->type === self::TOURIST_TYPE;    
    }

    // Función que determina si un Usuario es default o no
    public function isDefault()    {        
        return $this->type === self::DEFAULT_TYPE;    
    }



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
