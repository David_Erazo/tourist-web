<?php

use Illuminate\Database\Seeder;
use App\User;

class UserInitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= new User();
        $user->username='admin';
        $user->email='noreply@aplaplac.cl';
        $user->password=bcrypt('456827abcde');
        $user->type='default';
        $user->save();
    }
}
